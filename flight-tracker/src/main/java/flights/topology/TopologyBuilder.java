package flights.topology;

import flights.serde.Serde;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import radar.AirportKpiOutput;
import radar.AirportUpdateEvent;
import radar.FlightUpdateEvent;

public class TopologyBuilder implements Serde {

    private Properties config;

    private static AirportKpiOutput convertToAirportKpiOutput(FlightUpdateEvent flight) {
        // [to, string] - Extract start place from destination column.
        // System.out.println(flight.getDestination().toString());
        String from = flight.getDestination().toString().split("->")[0];
        from = from.split("\\(")[0];
        String to = flight.getDestination().toString().split("->")[1];
        to = to.split("\\(")[0];

        // [departureTimestamp, long] - Rename field STD to departureTimestamp.
        Long departureTimestamp = flight.getSTD();

        // [arrivalTimestamp, long] - Rename field STA to arrivalTimestamp.
        Long arrivalTimestamp = flight.getSTA();

        // [duration, long] - Calculate duration of a flight in minutes.
        // arrivalTimestamp - departureTimestamp
        Long duration = arrivalTimestamp - departureTimestamp;
        // to minutes
        duration = duration / 60000;

        // [departureDatetime, string] - Transform STD to ISO 8601 format using the
        // timezone of destination defined in the timezones column of input event.

        String timezoneDestination = flight.getTimezones().toString().split("->")[0];
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        dateFormat.setTimeZone(java.util.TimeZone.getTimeZone(timezoneDestination));
        String departureDatetime = dateFormat.format(departureTimestamp);

        // [arrivalDatetime, string] - Transform STA timestamp to ISO 8601 format using
        // the timezone of arrival destination defined in the timezones column of input
        // event.
        String timezoneArrival = flight.getTimezones().toString().split("->")[1];
        dateFormat.setTimeZone(java.util.TimeZone.getTimeZone(timezoneArrival));
        String arrivalDatetime = dateFormat.format(arrivalTimestamp);

        // [departureAirportCode, string] - Extract departure airport code from
        // destination column of input event.
        String departureAirportCode = flight.getDestination().toString().split("->")[0];
        departureAirportCode = departureAirportCode.split("\\(")[1];
        departureAirportCode = departureAirportCode.split("\\)")[0];

        // [arrivalAirportCode, string] - Extract arrival airport code from destination
        // column of input event.
        String arrivalAirportCode = flight.getDestination().toString().split("->")[1];
        arrivalAirportCode = arrivalAirportCode.split("\\(")[1];
        arrivalAirportCode = arrivalAirportCode.split("\\)")[0];

        // The rest of the fields are preserved except for the destination, STA, STD and
        // timezones fields. Transformed flights should be published to the output topic
        // radar.flights

        // flight.setId
        // flight.setDate
        // flight.setGate
        // flight.setStatus
        // flight.setAirline

        String id = flight.getId().toString();
        String date = flight.getDate().toString();
        String gate = flight.getGate().toString();
        String status = flight.getStatus().toString();
        String airline = flight.getAirline().toString();

        AirportKpiOutput output = new AirportKpiOutput(id, from, to, departureTimestamp, arrivalTimestamp,
                duration, departureDatetime, arrivalDatetime, departureAirportCode, arrivalAirportCode,
                date, gate, status, airline);

        return output;
    }

    public TopologyBuilder(Properties properties) {
        this.config = properties;
    }

    private static final Logger logger = LogManager.getLogger(TopologyBuilder.class);

    public Topology build() {
        StreamsBuilder builder = new StreamsBuilder();
        String schemaRegistry = config.getProperty("kafka.schema.registry.url");

        KStream<String, FlightUpdateEvent> flightInputStream = builder.stream(
                config.getProperty("kafka.topic.flight.update.events"),
                Consumed.with(Serde.stringSerde, Serde.specificSerde(FlightUpdateEvent.class, schemaRegistry)));

        GlobalKTable<String, AirportUpdateEvent> airportTable = builder.globalTable(
                config.getProperty("kafka.topic.airport.update.events"),
                Consumed.with(Serde.stringSerde, Serde.specificSerde(AirportUpdateEvent.class, schemaRegistry)));

        // AirportKpiOutput.avsc
        // ZAD 1 start
        flightInputStream
                .filter((key, flight) -> !flight.getStatus().toString().equals("CANCELED"))
                // .peek((key, flight) -> System.out.println(flight.getStatus().toString()))
                .mapValues(flight -> {
                    AirportKpiOutput output = convertToAirportKpiOutput(flight);

                    return output;
                }).to("radar.flights",
                        Produced.with(Serde.stringSerde, Serde.specificSerde(AirportKpiOutput.class,
                                schemaRegistry)));
        // NOTE: OVO RADI
        // ZAD 1 end

        // flightInputStream
        // .filter((key, flight) -> !flight.getStatus().toString().equals("CANCELED"))
        // .filter((key, flight) -> !flight.getStatus().toString().equals("LATE"))
        // .mapValues(flight -> {
        // AirportKpiOutput output = convertToAirportKpiOutput(flight);

        // return output;
        // }).windowedBy(TimeWindows.of(Duration.ofMinutes(5))).count().toStream()
        // .peek((key, value) -> System.out.println(key + " " + value))
        // .to("radar.flights.count");

        return builder.build();
    }
}
